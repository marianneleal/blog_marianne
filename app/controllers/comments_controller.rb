# durch das nesting: mehr Komplexität hier als in ArticlesController;

class CommentsController < ApplicationController
  http_basic_authenticate_with name: "marianne", password: "password", only: :destroy
  
  def create
    #Each request for a comment has to keep track of the article 
    #to which the comment is attached, thus the initial call to the 
    #find method of the Article model to get the article in question.
    @article = Article.find(params[:article_id])
    
    #create auf @article.comments nutzen,
    # um comment zu createn und zu saven.
    # linkt automatisch comment sodass es zum jeweiligen artikel gehört.
    @comment = @article.comments.create!(comment_params)
    # danach user zurückschicken zum originalen article mit article_path(@article) helper.
    # dies ruft die show action in ArticlesController auf, die die view show.html.erb rendert.
    # dort soll der comment auftauchen, deshalb nochmal show bearbeiten.
    redirect_to article_path(@article)
  end

  # deletes comment by locating it within @article.comments collection, then removing it from the db
  # and sending us back to the show action for the article
  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article)
  end

  private
    def comment_params
      params.require(:comment).permit(:commenter, :body, :status)
    end
end
