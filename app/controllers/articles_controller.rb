class ArticlesController < ApplicationController

  http_basic_authenticate_with name: "marianne", password: "password", except: [:index, :show]

  def index
    @articles = Article.all
  end

#new action: show, ruft Article.find mit der id auf, die
# in routes.rb als route parameter gesetzt wurde.
# returned Article wird in der @article instanzvariable gespeichert,
# ist also von der view accessible
  def show
    @article = Article.find(params[:id])
  end

  #new instanziiert neuen article, aber speichert ihn nicht.
  # dieser neue article wird in der view benutzt, wenn die form
  # gebaut wird. 
  def new
    @article = Article.new
  end

  #create instanziiert neuen article mit parametern für titel und
  #body, versucht, diesen zu speichern. wenn speichern klappt:
  # redirected create den browser zur seite des artikels bei
  # "http://localhost:3000/articles/#{@article.id}". 
  # wenn speichern nicht klappt:
  # render new, also app/views/articles/new.htlm.erb wird gerendert
  
  def create
    #parameter: article_params unten privately definiert
    @article = Article.new(article_params)

    if @article.save
      redirect_to @article
    else
      render :new
    end
  end

  # fetches article from db, stores it in @article so that it can be used
  # when building the form. renders app/views/articles/edit.html.erb by default
  def edit
    @article = Article.find(params[:id])
  end


# (re-)fetches article from db, attempts to update it with the submitted
# data filtered by article_params. 
# if successful: action redirects the browser to the article's page
# else: redisplays the form with error msgs by rendering 
# app/views/articles/edit.html.erb

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to root_path
  end

  private
  
  def article_params
    params.require(:article).permit(:title, :body, :status)
  end

  def comment_params
    params.require(:comment).permit(:commenter, :body, :status)
  end


end

