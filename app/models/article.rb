class Article < ApplicationRecord
  include Visible
  # 2. Seite der Assoziation zu einem anderen Model,
  # in diesem Fall: comment.rb (hat belongs_to :article),
  # article hat has_many :comments, sind damit assoziiert.
  # dependent: dependents des articles (die kommentare), :destroy: löschen
  has_many :comments, dependent: :destroy

  # a title value must be present (String, also mind 1 ' ' character)
  validates :title, presence: true
  # a body value must be present and at least 10 characters long.
  validates :body, presence: true, length: { minimum: 10 }

end
  

