Rails.application.routes.draw do
  root "articles#index"
# resources-Methode mappt alle coventional routes für eine collection
#von resources, zB articles, auf diese 
  resources :articles do
    #This creates comments as a nested resource within articles. 
    # This is another part of capturing the hierarchical relationship 
    # that exists between articles and comments.
    resources :comments
  end
end
