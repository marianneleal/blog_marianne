class CreateArticles < ActiveRecord::Migration[6.1]
  def change
    # create_table specifies how the articles table should be
    # constructed.
    # default: adds an id column as an auto-incrementing primary
    # key (first record in table = id 1, snd ed 2, usw.)
    create_table :articles do |t|
      # title : string und body : text aus dem generate model command
      t.string :title
      t.text :body
      # t.timestamps defines 2 additional columns named
      # created_at and updated_at. Rails will manage these for us,
      # setting the values when we create or update a model object.
      t.timestamps
    end
  end
end

#run migration: bin/rails db:migrate, after that: interact with tables
# using our model.
