class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.string :commenter
      t.text :body
      # t.reference kreiert integer spalte namens article_id,
      # einen index dafür und einen foreign key constraint, der
      # auf die id spalte in der articles tabelle zeigt.
      t.references :article, null: false, foreign_key: true

      t.timestamps
    end
  end
end
